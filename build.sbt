organization := "com.altuera"
name := "top-topics"
version := "0.0.2"

scalaVersion := "2.12.8"

mainClass in Compile := Some("com.altuera.toptopics.Servlet")

scalacOptions += "-Ypartial-unification" // 2.11.9+
scalacOptions += "-feature"

libraryDependencies ++= Seq(

	"javax.servlet" % "javax.servlet-api" % "3.1.0" % "provided",
	"mysql" % "mysql-connector-java" % "8.0.13",
	"ch.qos.logback" % "logback-classic" % "1.2.3",
	"com.typesafe" % "config" % "1.3.3",
	"com.zaxxer" % "HikariCP" % "3.3.0",
	"io.spray" %% "spray-json" % "1.3.5"
)

//если хотите версию томката которая не совпадает с версией по умолчанию
//containerLibs in Tomcat := Seq("com.github.jsimone" % "webapp-runner" % "7.0.34.1" intransitive())

enablePlugins(JettyPlugin, TomcatPlugin)

fork in run := true
