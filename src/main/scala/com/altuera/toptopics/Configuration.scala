package com.altuera.toptopics

import com.typesafe.config.{Config, ConfigFactory}

object Configuration {

	private val config = ConfigFactory.load()

	def configDb: Config = config.getConfig("db")
	
	def schedulerPollingPeriodInMilliseconds: Long = Some("scheduler.pollingPeriodInMilliseconds").filter(config.hasPath).map(config.getLong).getOrElse(20000)

	def clientPollingPeriodInMilliseconds: Long = Some("client.pollingPeriodInMilliseconds").filter(config.hasPath).map(config.getLong).getOrElse(20000)

}
