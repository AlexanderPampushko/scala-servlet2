package com.altuera.toptopics

import java.sql._
import java.util.Properties

import com.altuera.toptopics.Model.Top10_Subproduct
import com.typesafe.config.Config
import com.zaxxer.hikari.{HikariConfig, HikariDataSource}
import javax.sql.DataSource
import org.slf4j.LoggerFactory

import scala.collection.JavaConverters._

object DataBase {

	private val logger = LoggerFactory.getLogger(this.getClass)

	private val dataSource = createDataSource(Configuration.configDb)

	private def createDataSource(config: Config): DataSource = {
		try {

			new HikariDataSource(makeHikariConfig(config))

		} catch {
			case t: Throwable => {
				logger.error(s"Can't connect to db. Config = $config", t)
				throw t
			}
		}
	}

	private def makeHikariConfig(config: Config): HikariConfig = {

		val properties = new Properties()
		val cfg: Map[String, Object] = config.entrySet.asScala.map { entry => (entry.getKey -> entry.getValue.unwrapped) }.toMap
		properties.putAll(cfg.asJava)

		new HikariConfig(properties)
	}

	def getExamples: List[Top10_Subproduct] = {
		try {
			withDBConnection(dataSource.getConnection)(connection => {
				val statement = connection.prepareStatement("select THEME, COUNT from TOP10_SUBPRODUCT;")
				val resultSet = statement.executeQuery()
				val list = toList({ r => Top10_Subproduct(r.getString(1), r.getLong(2)) }, resultSet)
				connection.commit()
				list
			})

		} catch {
			case t: Throwable => {
				logger.warn("Can't load data from examples table", t)
				throw t
			}
		}
	}

	private def toList[A](convert: ResultSet => A, resultSet: ResultSet) = {
		new Iterator[ResultSet] {
			override def hasNext: Boolean = resultSet.next()

			override def next(): ResultSet = resultSet
		}.map(convert).toList
	}

	def withDBConnection[A](connection: Connection)(block: Connection => A): A = {
		try {
			block(connection)
		} catch {
			case e: Throwable => {
				logger.error("Error in DB", e)
				throw e
			}
		} finally {
			connection.close()
		}
	}
}

