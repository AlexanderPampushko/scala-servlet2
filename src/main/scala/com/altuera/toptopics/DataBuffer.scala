package com.altuera.toptopics

object DataBuffer {

	private var data: List[Model.Top10_Subproduct] = List()

	def getData: List[Model.Top10_Subproduct] = data

	def setData(value: List[Model.Top10_Subproduct]): Unit = {
		data = value
	}
}
