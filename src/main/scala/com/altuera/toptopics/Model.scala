package com.altuera.toptopics

import spray.json.{DefaultJsonProtocol, _}

trait Model {

}

object Model {

	case class Top10_Subproduct(theme: String, count: Long) extends Model

	final case class ListItem(id: Long, keyName: Long, keyValue: String) extends Model

	sealed trait ListType {
		def name: String
	}

	object ModelProtocol extends DefaultJsonProtocol {
		implicit val Top10_SubproductFormat = jsonFormat2(Top10_Subproduct)

		def toJson(m: Model): JsValue = {
			m match {
				case g: Top10_Subproduct => g.toJson
			}
		}
	}

	def toJson(m: Model): JsValue = {
		ModelProtocol.toJson(m)
	}
}
