package com.altuera.toptopics

import java.util.concurrent.{ScheduledThreadPoolExecutor, TimeUnit}

import org.slf4j.LoggerFactory

class Scheduler() {

	private val log = LoggerFactory.getLogger(this.getClass)

	private var executor: ScheduledThreadPoolExecutor = _

	private def updateDataFromSql(): Unit = {
		try {
			DataBuffer.setData(DataBase.getExamples)
			log.trace("databuffer updated")
		}
		catch {
			case ex: Exception => {
				log.error("Task execution error (read data from SQL server)", ex)
			}
		}
	}

	def start(): Unit = {

		executor = new ScheduledThreadPoolExecutor(1)
		val pollingPeriodInMilliseconds = Configuration.schedulerPollingPeriodInMilliseconds

		// Make sure the polling period does not exceed 1 request per second.
		val normalizedPollingPeriod = Math.max(1000, pollingPeriodInMilliseconds)

		var x = executor.scheduleAtFixedRate(new Thread("Polling thread") {
			override def run(): Unit = {
				updateDataFromSql()
			}
		}, 1, normalizedPollingPeriod, TimeUnit.MILLISECONDS)

	}

	def stop(): Unit = {
		if (executor != null) {
			executor.shutdown()
		}
	}

}
