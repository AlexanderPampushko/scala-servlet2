package com.altuera.toptopics

import com.altuera.toptopics.Model.ModelProtocol._
import javax.servlet.ServletConfig
import javax.servlet.annotation.WebServlet
import javax.servlet.http.{HttpServlet, HttpServletRequest, HttpServletResponse}
import org.slf4j.LoggerFactory
import spray.json.{JsObject, _}

@WebServlet(name = "TopTopicsServlet", urlPatterns = {
	Array("/json/top-topics/*")
}, loadOnStartup = 1)
class TopTopicsServlet extends HttpServlet {

	private val log = LoggerFactory.getLogger(this.getClass)

	private var scheduler: Scheduler = _

	override def init(config: ServletConfig) = {

		scheduler = new Scheduler()
		scheduler.start()
	}

	override def doGet(req: HttpServletRequest, resp: HttpServletResponse) = {

		resp.setContentType("application/json")
		resp.setCharacterEncoding("UTF-8")

		resp.setStatus(200)

		log.trace("send data to client")

		val result = try {
			val data = DataBuffer.getData
			JsObject("data" -> data.toJson, "clientPollingPeriodInMilliseconds" -> JsNumber(Configuration.clientPollingPeriodInMilliseconds))
		}
		catch {
			case t: Throwable => JsObject("result" -> JsString("error"), "message" -> JsString(t.getLocalizedMessage))
		}

		resp.getOutputStream.print(result.toString())

	}

	override def destroy() = {
		scheduler.stop()
	}
}
